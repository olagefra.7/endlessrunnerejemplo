extends "ScrollMovement.gd"
onready var animation = $AnimatedSprite
onready var death_sound = $ExplosionSound

func _physics_process(delta):
	move(5.0)


func _on_Obstacle_body_entered(body):
	if body.name == "Player":
		death_sound.play()
		animation.play("Explotar")
		yield(get_node("AnimatedSprite"), "animation_finished")
		queue_free()
		Signals.emit_signal("killplayer")


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
