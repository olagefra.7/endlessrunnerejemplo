extends KinematicBody2D

var velocity = Vector2.ZERO

export var jump_velocity = 1500.0
#export var gravity_scale = 2

var score = 0
var vidas = 3

var can_jump: bool = true

onready var jump_sound = $JumpSound
onready var death_sound = $DeathSound

onready var animation = $AnimatedSprite

var move_y = 0

func _ready():
	Signals.connect("killplayer",self,"killplayer")
	Signals.connect("rewardplayer",self,"rewardplayer")
	Signals.connect("quitarvida",self,"quitarvida")
	animation.play("Run")
	
	
func _physics_process(delta):
	move_y += 15

	if is_on_floor():
		move_y = 0
		
		if Input.is_action_pressed("jump"):
			move_y = -500
			animation.play("Jump")
			jump_sound.play()
			
	move_and_slide(Vector2(0, move_y), Vector2(0, -1))	
	
	
	
		
#func _input(event):
#	velocity = Vector2.ZERO
#	if can_jump:
#		if event.is_action_pressed("jump"):
#			velocity.y-=jump_velocity
#			animation.play("Jump")
#			jump_sound.play()
		

func _on_Area2D_body_entered(body):
		
	if body is StaticBody2D:
		can_jump = true
		animation.play("Run")
		
	


func _on_Area2D_body_exited(body):
	if body is StaticBody2D:
		can_jump = false
		
func quitarvida(vida):
	death_sound.play()
	animation.play("Choque")
	vidas-=vida
	print(vidas)
	if vidas == 0:
		yield(get_node("AnimatedSprite"), "animation_finished")
		get_tree().change_scene("res://Perder.tscn")
	Signals.emit_signal("updatevidas",vidas)
	yield(get_node("AnimatedSprite"), "animation_finished")
	animation.play("Run")
	

func killplayer():
	queue_free()
	get_tree().change_scene("res://Perder.tscn")
	
func rewardplayer(scoretoadd):
	score+=scoretoadd

	Signals.emit_signal("updatescore",score)
	if score == 25:
		get_tree().change_scene("res://Ganar.tscn")

func _on_Area2D_area_entered(area):
	if area.name == "Pickup":
		animation.play("Disparo")
		yield(get_node("AnimatedSprite"), "animation_finished")
		animation.play("Run")
