extends "ScrollMovement.gd"

onready var pickup_sound = $PickupSound
onready var animation = $AnimatedSprite

func _physics_process(delta):
	move(5.0)
	

func _on_Pickup_body_entered(body):
	if body.name == "Player":
		animation.play("abducir")
		Signals.emit_signal("rewardplayer",1)
		pickup_sound.play()
		yield(pickup_sound,"finished")
		queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
